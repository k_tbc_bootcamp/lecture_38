package com.example.lecture_38

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainViewModel : ViewModel() {

    val progressBarVisible = MutableLiveData<Boolean>()
    val result = MutableLiveData<String>()
    val successColor = MutableLiveData<Boolean>()
    val isClickable = MutableLiveData<Boolean>().apply {
        value = true
    }

    fun logIn(email: String, password: String) {
        progressBarVisible.value = true
        isClickable.value = false
        Handler().postDelayed({
            progressBarVisible.value = false
            isClickable.value = true
            if (email.isNotEmpty() && password.isNotEmpty()) {
                successColor.value = true
                result.value = "Logged in successfully"
            } else {
                successColor.value = false
                result.value = "Could not log in"
            }
        }, 3000)
    }

}