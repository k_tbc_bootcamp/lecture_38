package com.example.lecture_38

import android.graphics.Color
import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter

@BindingAdapter("visibility")
fun View.setVisibility(visible: Boolean) {
    visibility = if (visible) View.VISIBLE else View.GONE
}

@BindingAdapter("successColor")
fun TextView.setSuccessColor(success: Boolean) {
    if (success)
        setTextColor(Color.parseColor("#14d34b"))
    else
    //meore xerxic maqvs gamoyenebuli mravalferovnebistvis
        setTextColor(resources.getColor(android.R.color.holo_red_dark))
}